package com.ourbook.ourbookV3.repositories;

import com.ourbook.ourbookV3.models.ChatMembership;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChatMembershipRepository extends JpaRepository<ChatMembership, Long> {
}
