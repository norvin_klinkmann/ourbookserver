package com.ourbook.ourbookV3.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.ourbook.ourbookV3.models.*;

import java.util.Optional;


/**
 *
 * @author norvin_klinkmann
 */

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

    Optional<Book> findBookByTitel(@Param("titel") String titel);

    Optional<Book> findByUser(@Param("user") User user);

}
