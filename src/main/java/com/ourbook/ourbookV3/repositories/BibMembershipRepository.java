package com.ourbook.ourbookV3.repositories;

import com.ourbook.ourbookV3.models.BibMembership;
import com.ourbook.ourbookV3.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 *
 * @author norvin_klinkmann
 */

@Repository
public interface BibMembershipRepository extends JpaRepository<BibMembership, Long> {

    Optional<BibMembership> findBibMembershipByUserId(@Param("userId") Long userId);

    Iterable<BibMembership> findAllByUserId(@Param("userId") Long userId);


}
