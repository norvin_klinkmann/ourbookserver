package com.ourbook.ourbookV3.repositories;

import com.ourbook.ourbookV3.models.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author norvin_klinkmann
 */

@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {
}
