package com.ourbook.ourbookV3.repositories;

import com.ourbook.ourbookV3.models.BibBook;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BibBookRepository extends JpaRepository<BibBook, Long> {
}
