package com.ourbook.ourbookV3.repositories;

import com.ourbook.ourbookV3.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 *
 * @author norvin_klinkmann
 */

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    boolean existsUserByUsername(@Param("username") String username);

    Optional<User> findUserByUsername(@Param("username") String username);

    Optional<User> findUserByPasswordResetToken(@Param("passwordResetToken") String passwordResetToken);

    Optional<User> findUserByUsernameOrEmail(String username, String email);
}
