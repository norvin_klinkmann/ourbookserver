package com.ourbook.ourbookV3.configuration;

import com.ourbook.ourbookV3.Services.AuthenticationProviderService;
import com.ourbook.ourbookV3.Services.MyUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.scrypt.SCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.session.SessionManagementFilter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;

@Configuration
public class SecurityConf extends WebSecurityConfigurerAdapter {

    @Autowired
    DataSource dataSource;

    @Autowired
    private AuthenticationProviderService authenticationProvider;

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public SCryptPasswordEncoder sCryptPasswordEncoder() {
        return new SCryptPasswordEncoder();
    }

//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception{
//        System.out.println("config Manager");
//        auth.jdbcAuthentication()
//                .dataSource(dataSource);
//
//    }
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        System.out.println("config HTTP");
//        http.authorizeRequests().anyRequest().authenticated();
//    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authenticationProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .httpBasic()
                .and()
                .formLogin().disable()
                .cors()
                .and()
                .csrf().disable()
                .authorizeRequests()
                .mvcMatchers("/changePassword/*").anonymous()
                .mvcMatchers("/changePassword/**").anonymous()
                .mvcMatchers("/resetPassword/**").anonymous()
                .mvcMatchers("/resetPassword/*").anonymous()
                .mvcMatchers("/user/register/**").anonymous()
                .mvcMatchers("/admin").hasAnyRole("ADMIN", "SUPERADMIN")
                .mvcMatchers("/changePasswordAdmin/**").hasAnyRole("ADMIN", "SUPERADMIN")
                .mvcMatchers("/changePasswordAdmin/*").hasAnyRole("ADMIN", "SUPERADMIN")
                .anyRequest().authenticated()
                .and()
                .logout()
                .logoutUrl("/perform_logout")
                .deleteCookies("JSESSIONID")
                .invalidateHttpSession(true)
                .logoutSuccessUrl("/");


        /*http
                .httpBasic()
                .and()
                .formLogin().disable()
                //.cors()
                //.and()
                //.csrf().disable()
                .authorizeRequests()
                .mvcMatchers("/changePassword/*").anonymous()
                .mvcMatchers("/changePassword/**").anonymous()
                .mvcMatchers("/resetPassword/**").anonymous()
                .mvcMatchers("/resetPassword/*").anonymous()
                .mvcMatchers("/user/register/**").anonymous()
                .mvcMatchers("/admin").hasAnyRole("ADMIN","SUPERADMIN")
                .mvcMatchers("/changePasswordAdmin/**").hasAnyRole("ADMIN","SUPERADMIN")
                .mvcMatchers("/changePasswordAdmin/*").hasAnyRole("ADMIN","SUPERADMIN")
                .anyRequest().authenticated()
                .and()
                .logout()
                .logoutUrl("/perform_logout")
                .deleteCookies("JSESSIONID");*/


/*
        http
                .httpBasic()
                .and()
                .formLogin().disable()
                .authorizeRequests().anyRequest().authenticated();
*/

//        http
//                .csrf().disable()
//                .authorizeRequests().anyRequest().authenticated()
//                .and()
//                .formLogin().disable()
//                .httpBasic();
//
//        http.formLogin()
//                .defaultSuccessUrl("/hello", true);
//        http.authorizeRequests().
//                mvcMatchers("/changePassword/*").anonymous().
//                mvcMatchers("/changePassword/**").anonymous().
//                mvcMatchers("/resetPassword/**").anonymous().
//                mvcMatchers("/resetPassword/*").anonymous().
//                mvcMatchers("/admin").hasAnyRole("ADMIN","SUPERADMIN").
//                mvcMatchers("/changePasswordAdmin/**").hasAnyRole("ADMIN","SUPERADMIN").
//                mvcMatchers("/changePasswordAdmin/*").hasAnyRole("ADMIN","SUPERADMIN").
//                anyRequest().
//                hasAnyAuthority("login","superadmin").
//                //authenticated()
//                and().
//                //formLogin().disable()*/
//                formLogin()
//                  .loginPage("/login")
//                  .loginProcessingUrl("/perform_login")
//                  .defaultSuccessUrl("/hello", true)
//                  .failureUrl("/login?error=true")
//                  .failureHandler(new AuthenticationFailureHandler() {
//                    @Override
//                    public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
//                        System.out.println("schlug fehl ");
//                    }
//               })
//                .and()
//                .logout()
//                .logoutUrl("/perform_logout")
//                .deleteCookies("JSESSIONID");

    }

}
