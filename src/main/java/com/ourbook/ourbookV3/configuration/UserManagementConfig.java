package com.ourbook.ourbookV3.configuration;

import com.ourbook.ourbookV3.models.Authority;
import com.ourbook.ourbookV3.models.Bib;
import com.ourbook.ourbookV3.models.User;
import com.ourbook.ourbookV3.repositories.*;
import com.ourbook.ourbookV3.Services.MyUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import java.util.Date;

@Configuration
public class UserManagementConfig {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BibRepository bibRepository;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    /**
     * Schaut nach ob es den User Eintrag mit username gibt. Erstellt einen neuen User,
     * wenn es ihn noch nicht gibt und weist dem neuen User die authorities zu.
     * @param username
     * @param password
     * @return
     */
    public User createUserIfNotExists(String username, String password, String[] authorities)
    {

        if (userRepository.existsUserByUsername(username)) {
            return userRepository.findUserByUsername(username).get();
        }

        String encodedPassword = (String) bCryptPasswordEncoder.encode(password);


        var user = new User();
        user.setUsername(username);
        user.setPassword(encodedPassword);
        user.setFirstname(username);
        user.setActive(true);

        userRepository.save(user);

        for (String auth : authorities) {
            var authority = new Authority();
            authority.setName(auth);
            authority.setUser(user);
            authorityRepository.save(authority);
        }

        return user;

    }

    @Bean
    public UserDetailsService userDetailsService() {

        var userDetailsService = new MyUserDetailsService(userRepository);

        createUserIfNotExists("ilker", "12345", new String[]{"read", "write", "invite", "login", "ROLE_SUPERADMIN"});
        createUserIfNotExists("nick", "12345", new String[]{"read","write", "invite", "login", "ROLE_SUPERADMIN"});
        createUserIfNotExists("norvin", "12345", new String[]{"read","write", "invite", "login", "ROLE_SUPERADMIN"});

        return userDetailsService;
        
    }

//    @Bean
//    public PasswordEncoder passwordEncoder() {
//        return NoOpPasswordEncoder.getInstance();
//    }
    
}
