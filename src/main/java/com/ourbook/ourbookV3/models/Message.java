package com.ourbook.ourbookV3.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author norvin_klinkmann
 */

@Entity
@Table(name = "Messages")
public class Message {

    @Id
    @GeneratedValue
    private Long id;

    private String message;
    private Long message_chatId;
    private Long message_senderId;
    private String message_timestemp;

    protected Message() {
        super();
    }

    public Message(Long id, String message, Long message_chatId, Long message_senderId, String message_timestemp) {
        super();
        this.id = id;
        this.message = message;
        this.message_chatId = message_chatId;
        this.message_senderId = message_senderId;
        this.message_timestemp = message_timestemp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getMessage_chatId() {
        return message_chatId;
    }

    public void setMessage_chatId(Long message_chatId) {
        this.message_chatId = message_chatId;
    }

    public Long getMessage_senderId() {
        return message_senderId;
    }

    public void setMessage_senderId(Long message_senderId) {
        this.message_senderId = message_senderId;
    }

    public String getMessage_timestemp() {
        return message_timestemp;
    }

    public void setMessage_timestemp(String message_timestemp) {
        this.message_timestemp = message_timestemp;
    }

}
