package com.ourbook.ourbookV3.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author norvin_klinkmann
 */

@Entity
@Table(name = "ChatMemberships")
public class ChatMembership {

    @Id
    @GeneratedValue
    private Long id;

    private Long chatId;
    private Long userId;

    public ChatMembership() {
        super();
    }

    public ChatMembership(Long id, Long chatId, Long userId) {
        super();
        this.id = id;
        this.chatId = chatId;
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getChatId() {
        return chatId;
    }

    public void setChatId(Long chatId) {
        this.chatId = chatId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long cm_userId) {
        this.userId = cm_userId;
    }
}
