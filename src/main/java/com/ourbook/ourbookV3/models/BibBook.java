package com.ourbook.ourbookV3.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author norvin_klinkmann
 */

@Entity
@Table(name = "BibBooks")
public class BibBook {

    @Id
    @GeneratedValue
    private Long id;

    private Long bibId;
    private Long bookId;

    public BibBook() {
        super();
    }

    public BibBook(Long id, Long bibId, Long bookId) {
        super();
        this.id = id;
        this.bibId = bibId;
        this.bookId = bookId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBibId() {
        return bibId;
    }

    public void setBibId(Long bibId) {
        this.bibId = bibId;
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }
}
