package com.ourbook.ourbookV3.models;

import org.springframework.web.client.RestTemplate;

public class RestBook {

    private String titel;
    private String isbn;
    private int status;
    private String authorName;
    private String erscheinungsdatum;
    private String sprache;
    private String auflage;

    public RestBook() {
    }

    public RestBook(String titel, String isbn) {
        this.titel = titel;
        this.isbn = isbn;
    }

    public RestBook(String titel, String isbn, int status, String authorName, String erscheinungsdatum, String sprache, String auflage) {
        this.titel = titel;
        this.isbn = isbn;
        this.status = status;
        this.authorName = authorName;
        this.erscheinungsdatum = erscheinungsdatum;
        this.sprache = sprache;
        this.auflage = auflage;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getErscheinungsdatum() {
        return erscheinungsdatum;
    }

    public void setErscheinungsdatum(String erscheinungsdatum) {
        this.erscheinungsdatum = erscheinungsdatum;
    }

    public String getSprache() {
        return sprache;
    }

    public void setSprache(String sprache) {
        this.sprache = sprache;
    }

    public String getAuflage() {
        return auflage;
    }

    public void setAuflage(String auflage) {
        this.auflage = auflage;
    }

    @Override
    public String toString() {
        return "RestBook{" +
                "titel='" + titel + '\'' +
                ", isbn='" + isbn + '\'' +
                ", status=" + status +
                ", authorName='" + authorName + '\'' +
                ", erscheinungsdatum='" + erscheinungsdatum + '\'' +
                ", sprache='" + sprache + '\'' +
                ", auflage='" + auflage + '\'' +
                '}';
    }
}
