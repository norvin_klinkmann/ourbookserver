package com.ourbook.ourbookV3.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author norvin_klinkmann
 */

@Entity
@Table(name = "BorrowingProcesses")
public class BorrowingProcess {

    @Id
    @GeneratedValue
    private Long id;

    private Long bp_chatId;
    private Long bp_bookOwnerId;
    private Long bp_bookLeiherId;
    private Long bp_bookId;
    private String bp_ausleihdatum;
    private String bp_rueckgabedatum;
    private int bp_status;

    protected BorrowingProcess() {
        super();
    }

    public BorrowingProcess(Long id, Long bp_chatId, Long bp_bookOwnerId, Long bp_bookLeiherId, Long bp_bookId, String bp_ausleihdatum, String bp_rueckgabedatum, int bp_status) {
        super();
        this.id = id;
        this.bp_chatId = bp_chatId;
        this.bp_bookOwnerId = bp_bookOwnerId;
        this.bp_bookLeiherId = bp_bookLeiherId;
        this.bp_bookId = bp_bookId;
        this.bp_ausleihdatum = bp_ausleihdatum;
        this.bp_rueckgabedatum = bp_rueckgabedatum;
        this.bp_status = bp_status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBp_chatId() {
        return bp_chatId;
    }

    public void setBp_chatId(Long bp_chatId) {
        this.bp_chatId = bp_chatId;
    }

    public Long getBp_bookOwnerId() {
        return bp_bookOwnerId;
    }

    public void setBp_bookOwnerId(Long bp_bookOwnerId) {
        this.bp_bookOwnerId = bp_bookOwnerId;
    }

    public Long getBp_bookLeiherId() {
        return bp_bookLeiherId;
    }

    public void setBp_bookLeiherId(Long bp_bookLeiherId) {
        this.bp_bookLeiherId = bp_bookLeiherId;
    }

    public Long getBp_bookId() {
        return bp_bookId;
    }

    public void setBp_bookId(Long bp_bookId) {
        this.bp_bookId = bp_bookId;
    }

    public String getBp_ausleihdatum() {
        return bp_ausleihdatum;
    }

    public void setBp_ausleihdatum(String bp_ausleihdatum) {
        this.bp_ausleihdatum = bp_ausleihdatum;
    }

    public String getBp_rueckgabedatum() {
        return bp_rueckgabedatum;
    }

    public void setBp_rueckgabedatum(String bp_rueckgabedatum) {
        this.bp_rueckgabedatum = bp_rueckgabedatum;
    }

    public int getBp_status() {
        return bp_status;
    }

    public void setBp_status(int bp_status) {
        this.bp_status = bp_status;
    }
}
