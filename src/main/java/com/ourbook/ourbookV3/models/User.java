package com.ourbook.ourbookV3.models;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author norvin_klinkmann
 */

@Entity
@Table(name = "User")
public class User {

    @Id
    @GeneratedValue
    private Long id;

    private String username = "NULL";
    private String password = "NULL";
    private String firstname = "NULL";
    private String lastname = "NULL";
    private String email = "NULL";
    private boolean active = false;
    private String passwordResetToken = "NULL";
    private java.util.Date passwordResetTokenDate = null;

    @OneToOne(mappedBy = "user", fetch = FetchType.EAGER)
    private Bib bib;

    //@Enumerated(EnumType.STRING)
    //private EncryptionAlgorithm algorithm;

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    private List<Authority> authorities;


    public User(){
        super();
    }

    public User(Long id, String username, String password, String firstname, String lastname, String email) {
        super();
        this.id = id;
        this.username = username;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
    }

    public User hidePrivateInfo(User user) {
        user.password = null;
        user.passwordResetToken = null;
        user.passwordResetTokenDate = null;
        user.bib = null;
        user.authorities = null;
        return user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String user_username) {
        this.username = user_username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String user_password) {
        this.password = user_password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String user_vorname) {
        this.firstname = user_vorname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String user_nachname) {
        this.lastname = user_nachname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String user_email) {
        this.email = user_email;
    }

//    public EncryptionAlgorithm getAlgorithm() {
//        return algorithm;
//    }
//
//    public void setAlgorithm(EncryptionAlgorithm algorithm) {
//        this.algorithm = algorithm;
//    }
//


    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<Authority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<Authority> authorities) {
        this.authorities = authorities;
    }

    public String getPasswordResetToken() {
        return passwordResetToken;
    }

    public void setPasswordResetToken(String passwordResetToken) {
        this.passwordResetToken = passwordResetToken;
    }

    public Date getPasswordResetTokenDate() {
        return passwordResetTokenDate;
    }

    public void setPasswordResetTokenDate(Date passwordResetTokenDate) {
        this.passwordResetTokenDate = passwordResetTokenDate;
    }

    public Bib getBib() {
        return bib;
    }

    public void setBib(Bib bib) {
        this.bib = bib;
    }

    @Override
    public String toString() {
        return String.format(
                "User[id=%d, username='%s', password='%s', vorname='%s', nachname='%s', email='%s']",
                id, username, password, firstname, lastname, email);
    }

    public boolean isValidUsername(String name)
    {

        // Regex to check valid username.
        String regex = "^[A-Za-z]\\w{5,29}$";

        // Compile the ReGex
        Pattern p = Pattern.compile(regex);

        // If the username is empty
        // return false
        if (name == null) {
            return false;
        }

        // Pattern class contains matcher() method
        // to find matching between given username
        // and regular expression.
        Matcher m = p.matcher(name);

        // Return if the username
        // matched the ReGex
        return m.matches();
    }

    public boolean isValidPassword(String password)
    {

        // Regex to check valid username.
        String regex = "^[0-9A-Za-z]\\w{8,29}$";

        // Compile the ReGex
        Pattern p = Pattern.compile(regex);

        // If the username is empty
        // return false
        if (password == null) {
            return false;
        }

        // Pattern class contains matcher() method
        // to find matching between given username
        // and regular expression.
        Matcher m = p.matcher(password);

        // Return if the username
        // matched the ReGex
        return m.matches();
    }
}

