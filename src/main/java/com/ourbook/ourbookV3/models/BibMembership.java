package com.ourbook.ourbookV3.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author norvin_klinkmann
 */

@Entity
@Table(name = "BibMemberships")
public class BibMembership {

    @Id
    @GeneratedValue
    private Long id;

    private Long bibId;
    private Long userId;
    private Integer kind;// 1=admin 2=mitglied

    public BibMembership(){
        super();
    }

    public BibMembership(Long id, Long bibId, Long userId, Integer kind) {
        super();
        this.id = id;
        this.bibId = bibId;
        this.userId = userId;
        this.kind = kind;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBibId() {
        return bibId;
    }

    public void setBibId(Long bibId) {
        this.bibId = bibId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public int getKind() {
        return kind;
    }

    public void setKind(Integer kind) {
        this.kind = kind;
    }
}
