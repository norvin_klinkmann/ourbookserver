package com.ourbook.ourbookV3.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author norvin_klinkmann
 */

@Entity
@Table(name = "Contacts")
public class Contact {

    @Id
    @GeneratedValue
    private Long id;

    private Long userIdA;
    private Long userIdB;

    public Contact() {
        super();
    }

    public Contact(Long id, Long userIdA, Long userIdB) {
        this.id = id;
        this.userIdA = userIdA;
        this.userIdB = userIdB;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserIdA() {
        return userIdA;
    }

    public void setUserIdA(Long userIdA) {
        this.userIdA = userIdA;
    }

    public Long getUserIdB() {
        return userIdB;
    }

    public void setUserIdB(Long userIdB) {
        this.userIdB = userIdB;
    }
}
