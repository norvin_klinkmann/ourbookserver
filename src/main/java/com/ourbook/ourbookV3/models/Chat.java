package com.ourbook.ourbookV3.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author norvin_klinkmann
 */

@Entity
@Table(name = "Chats")
public class Chat {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    public Chat() {
        super();
    }

    public Chat(Long id, String name) {
        super();
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
