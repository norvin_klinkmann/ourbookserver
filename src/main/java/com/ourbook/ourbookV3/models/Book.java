package com.ourbook.ourbookV3.models;

import javax.persistence.*;

/**
 *
 * @author norvin_klinkmann
 */

@Entity
@Table(name = "books")
public class Book {

    @Id
    @GeneratedValue
    private Long id;

    private String titel;
    private String isbn;
    private int status;
    private String authorName;
    private String erscheinungsdatum;
    private String sprache;
    private String auflage;

    @OneToOne
    private User user;

    public Book(){
        super();
    }

    public Book(Long id, String titel, String isbn, int status, String bookAuthorName, String erscheinungsdatum, String sprache, String auflage, User user) {
        super();
        this.id = id;
        this.titel = titel;
        this.isbn = isbn;
        this.status = status;
        this.authorName = bookAuthorName;
        this.erscheinungsdatum = erscheinungsdatum;
        this.sprache = sprache;
        this.auflage = auflage;
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String book_titel) {
        this.titel = book_titel;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String book_isbn) {
        this.isbn = book_isbn;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int book_status) {
        this.status = book_status;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String book_author_name) {
        this.authorName = book_author_name;
    }

    public String getErscheinungsdatum() {
        return erscheinungsdatum;
    }

    public void setErscheinungsdatum(String book_erscheinungsdatum) {
        this.erscheinungsdatum = book_erscheinungsdatum;
    }

    public String getSprache() {
        return sprache;
    }

    public void setSprache(String book_sprache) {
        this.sprache = book_sprache;
    }

    public String getAuflage() {
        return auflage;
    }

    public void setAuflage(String book_auflage) {
        this.auflage = book_auflage;
    }

    @Override
    public String toString() {
        return String.format(
                "Book[id=%d, book_titel='%s', book_isbn='%s', book_status='%d', book_author_name='%s', book_erscheinungsdatum='%s', book_sprache='%s', book_auflage='%s']",
                id, titel, isbn, status, authorName, erscheinungsdatum, sprache, auflage);
    }
}
