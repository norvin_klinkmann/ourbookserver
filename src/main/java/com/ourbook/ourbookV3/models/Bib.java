package com.ourbook.ourbookV3.models;

import javax.persistence.*;

/**
 *
 * @author norvin_klinkmann
 */

@Entity
@Table(name = "bibs")
public class Bib {

    @Id
    @GeneratedValue
    private Long id;

    private Integer kind; // 1 = normale Bib

    private String name;

    private Long chatId;

    @JoinColumn(name = "user")
    @OneToOne
    private User user;

    public Bib(){
        super();
    }

    public Bib(Long id, Integer kind, Long chatId, User user) {
        super();
        this.id = id;
        this.kind = kind;
        this.chatId = chatId;
        this.user = user;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getKind() {
        return kind;
    }

    public void setKind(Integer kind) {
        this.kind = kind;
    }

    public Long getChatId() {
        return chatId;
    }

    public void setChatId(Long chatId) {
        this.chatId = chatId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Bib{" +
                "id=" + id +
                ", kind=" + kind +
                ", chatId=" + chatId +
                ", user=" + user +
                '}';
    }

}
