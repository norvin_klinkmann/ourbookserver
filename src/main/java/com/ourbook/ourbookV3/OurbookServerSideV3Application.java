package com.ourbook.ourbookV3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
@EnableWebSecurity(debug = true)
public class OurbookServerSideV3Application {

	public static void main(String[] args) {
		SpringApplication.run(OurbookServerSideV3Application.class, args);
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {

			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**").allowedOrigins("*"); //wäre cool aber funktioniert nicht mit cookies
				//registry.addMapping("/**").allowedOrigins("http://localhost:3000");
				//registry.addMapping("/**").allowedOrigins("http://localhost:3000").allowCredentials(true).maxAge(3600);

			}
		};
	}

}
