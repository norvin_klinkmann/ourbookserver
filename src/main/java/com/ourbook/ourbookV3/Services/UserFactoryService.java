package com.ourbook.ourbookV3.Services;

import com.ourbook.ourbookV3.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserFactoryService {

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    public void setPassword(User user, String password) throws Exception {
        if (password == null || password.length() < 8) {
            throw new Exception("Bad password.");
        }
        user.setPassword(bCryptPasswordEncoder.encode(password));
    }
}
