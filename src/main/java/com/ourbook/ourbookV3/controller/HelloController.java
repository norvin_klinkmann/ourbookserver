package com.ourbook.ourbookV3.controller;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    SecurityContextHolder holder;

    @GetMapping("/hello")
    public String hello() {
        return "Hello, Ourbook!";
    }
}
