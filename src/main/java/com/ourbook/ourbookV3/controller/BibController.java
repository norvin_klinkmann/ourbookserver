package com.ourbook.ourbookV3.controller;

import com.ourbook.ourbookV3.error.BibNotFoundException;
import com.ourbook.ourbookV3.models.*;
import com.ourbook.ourbookV3.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author norvin_klinkmann
 */

@RestController
public class BibController {

    @Autowired
    BibRepository bibRepository;

    @Autowired
    BibMembershipRepository bibMembershipRepository;

    @Autowired
    ChatRepository chatRepository;

    @Autowired
    ChatMembershipRepository chatMembershipRepository;

    @Autowired
    UserRepository userRepository;

    // Get All Notes
    @GetMapping("/bibs")
    public List<Bib> getAllNotes() {
        return bibRepository.findAll();
    }

    // Create a new Note
    @PostMapping("/bibs/createBib")
    public Bib createNote(@Valid @RequestBody Long creatorUserId, String name, List<Long> memberIds, Integer kind, Authentication authentication) {
        
        UserDetails userPrincipal = (UserDetails)authentication.getPrincipal();
        System.out.println("User principal name =" + userPrincipal.getUsername());
        System.out.println("Is user enabled =" + userPrincipal.isEnabled());
        System.out.println("Working for managers. Principal name = " + authentication.getName());

        Chat chat = new Chat();
        chat.setName(name);
        chatRepository.save(chat);
        Bib bib = new Bib();
        Optional<User> creatorUser = userRepository.findById(creatorUserId);
        bib.setUser(creatorUser.get());
        bib.setKind(1); // 1 = normale Bib
        bib.setChatId(chat.getId());
        bibRepository.save(bib);
        ChatMembership chatMembership = new ChatMembership();
        chatMembership.setChatId(chat.getId());
        chatMembership.setUserId(creatorUserId);
        chatMembershipRepository.save(chatMembership);
        BibMembership bibMembership = new BibMembership();
        bibMembership.setBibId(bib.getId());
        bibMembership.setUserId(creatorUserId);
        bibMembership.setKind(1); // 1=admin 2=mitglied
        bibMembershipRepository.save(bibMembership);
        for (Long memberId : memberIds) {
            BibMembership bibMembershipTemp = new BibMembership();
            bibMembershipTemp.setBibId(bib.getId());
            bibMembershipTemp.setUserId(memberId);
            bibMembershipTemp.setKind(2);
            bibMembershipRepository.save(bibMembershipTemp);
            ChatMembership chatMembershipTemp = new ChatMembership();
            chatMembershipTemp.setChatId(chat.getId());
            chatMembershipTemp.setUserId(memberId);
            chatMembershipRepository.save(chatMembershipTemp);
        }
        return bib;
    }

    @PostMapping("/bibs/addMember")
    public BibMembership addMember(@Valid @RequestBody Long bibId, Long newMemberId, Integer kind) {
        BibMembership bibMembership = new BibMembership();
        bibMembership.setBibId(bibId);
        bibMembership.setUserId(newMemberId);
        bibMembership.setKind(kind);
        return bibMembershipRepository.save(bibMembership);
    }

    // Get a Single Note
    @GetMapping("/bibs/{id}")
    public Bib getNoteById(@PathVariable(value = "id") Long bibId) throws BibNotFoundException {
        return bibRepository.findById(bibId)
                .orElseThrow(() -> new BibNotFoundException(bibId));
    }

    // Update a Note
    @PutMapping("/bibs/{id}")
    public Bib updateNote(@PathVariable(value = "id") Long bibId,
                           @Valid @RequestBody Bib bibDetails) throws BibNotFoundException {

        Bib bib = bibRepository.findById(bibId)
                .orElseThrow(() -> new BibNotFoundException(bibId));

        bib.setKind(bibDetails.getKind());
        bib.setChatId(bibDetails.getChatId());

        Bib updatedBib = bibRepository.save(bib);

        return updatedBib;
    }

    // Delete a Note
    @DeleteMapping("/bibs/{id}")
    public ResponseEntity<?> deleteBib(@PathVariable(value = "id") Long bibId) throws BibNotFoundException {
        Bib bib = bibRepository.findById(bibId)
                .orElseThrow(() -> new BibNotFoundException(bibId));

        bibRepository.delete(bib);

        return ResponseEntity.ok().build();
    }
}
