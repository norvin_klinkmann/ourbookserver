package com.ourbook.ourbookV3.controller;

import com.ourbook.ourbookV3.configuration.UserManagementConfig;
import com.ourbook.ourbookV3.models.*;
import com.ourbook.ourbookV3.error.*;
import com.ourbook.ourbookV3.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

/**
 *
 * @author norvin_klinkmann
 */

@RestController
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    BibMembershipRepository bibMembershipRepository;

    @Autowired
    ContactRepository contactRepository;

    @Autowired
    BibRepository bibRepository;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    // Get All Notes
    @GetMapping("/user")
    public Iterable<User> getAllNotes() {
        return userRepository.findAll();
    }

    // Create a new Note - register a new user
    @PostMapping("/user/register")
    public User createNote(@Valid @RequestBody User user) throws UserAlreadyExistsException, ExceptionBlueprint {
        String newUserName = user.getUsername();
        if (userRepository.existsUserByUsername(newUserName)) {
            throw new ExceptionBlueprint("usernamealreadyexists","nein",2);
        }
        else {
            User newUser = new User();
            if (user.isValidUsername(user.getUsername())) {
                if (user.isValidPassword(user.getPassword())){
                    newUser.setUsername(user.getUsername());
                    String encodedPassword = (String) bCryptPasswordEncoder.encode(user.getPassword());
                    newUser.setPassword(encodedPassword);
                    newUser.setFirstname(user.getFirstname());
                    newUser.setLastname(user.getLastname());
                    newUser.setEmail(user.getEmail());
                    userRepository.save(newUser);
                    String[] authorities = new String[]{"read", "write", "invite", "login"};
                    for (String auth : authorities) {
                        var authority = new Authority();
                        authority.setName(auth);
                        authority.setUser(newUser);
                        authorityRepository.save(authority);
                    }
                    Bib bib = new Bib();
                    bib.setUser(newUser);
                    bib.setKind(2); // 1 = user Bib
                    bib.setChatId(null);
                    bibRepository.save(bib);
                    BibMembership bibMembership = new BibMembership();
                    bibMembership.setBibId(bib.getId());
                    bibMembership.setUserId(newUser.getId());
                    bibMembership.setKind(1); // 1=admin 2=mitglied
                    bibMembershipRepository.save(bibMembership);
                    return newUser;
                } else {
                    throw new ExceptionBlueprint("invalidpassword","nein",1);
                }
            } else {
                throw new ExceptionBlueprint("invalidusername","nein",1);
            }

        }
    }
    
    @PostMapping("/user/newContactByUsername")
    public ResponseEntity<?> newContact(@Valid @RequestBody Long userIdA, String username) {
        if (userRepository.existsUserByUsername(username)) {
            User userB = userRepository.findUserByUsername(username).get();
            Contact contact = new Contact();
            contact.setUserIdA(userIdA);
            contact.setUserIdB(userB.getId());
            contactRepository.save(contact);
            return ResponseEntity.ok().build();
        } else return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    // Get a Single Note
    @GetMapping("/user/getUserById/{id}")
    public User getUserById(@PathVariable(value = "id") Long userId) throws UserNotFoundException {
        User user = new User();
        return user.hidePrivateInfo(userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException(userId)));
//        return userRepository.findById(userId)
//                .orElseThrow(() -> new UserNotFoundException(userId));
    }

    @GetMapping("/user/getUserByName/{name}")
    public User getUserByName(@PathVariable(value = "name") String username) throws UserNotFoundException {
        User user = new User();
        return user.hidePrivateInfo(userRepository.findUserByUsername(username)
                .orElseThrow(() -> new UserNotFoundException("user with name: "+username+ "not found")));
//        return userRepository.findUserByUsername(username)
//                .orElseThrow(() -> new UserNotFoundException("user with name: "+username+ "not found"));
    }

    // Update a Note
    @PutMapping("/user/{id}")
    public User updateNote(@PathVariable(value = "id") Long userId,
                           @Valid @RequestBody User userDetails) throws UserNotFoundException {

        User user = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException(userId));

        user.setUsername(userDetails.getUsername());
        user.setPassword(userDetails.getPassword());
        user.setFirstname(userDetails.getFirstname());
        user.setLastname(userDetails.getLastname());
        user.setEmail(userDetails.getEmail());

        return userRepository.save(user);
    }

    // Delete a Note
    @DeleteMapping("/user/{id}")
    public ResponseEntity<?> deleteBook(@PathVariable(value = "id") Long userId) throws UserNotFoundException {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException(userId));

        userRepository.delete(user);
        return ResponseEntity.ok().build();
    }
}
