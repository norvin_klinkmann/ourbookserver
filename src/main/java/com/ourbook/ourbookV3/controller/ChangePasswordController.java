package com.ourbook.ourbookV3.controller;

import com.ourbook.ourbookV3.Services.UserFactoryService;
import com.ourbook.ourbookV3.error.UserNotFoundException;
import com.ourbook.ourbookV3.models.User;
import com.ourbook.ourbookV3.repositories.UserRepository;
import com.ourbook.ourbookV3.utils.ReturnError;
import com.ourbook.ourbookV3.utils.ReturnSuccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Calendar;
import java.util.Date;

@RestController
public class ChangePasswordController {

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserFactoryService userFactory;


    @GetMapping("/changePasswordAdmin/{name}/{newPassword}")
    public Object changePasswordAdmin(@PathVariable(value="name") String name, @PathVariable(value="newPassword") String newPassword) {
        try {

            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

            System.out.println("authentication: "+authentication.getName());
//            (User) authentication.getPrincipal();

            User user = userRepository.findUserByUsernameOrEmail(name,name).orElseThrow(()->new UserNotFoundException("username or email not found: "+name));

            userFactory.setPassword(user, newPassword);

            user.setPasswordResetToken(null);
            user.setPasswordResetTokenDate(null);

            userRepository.save(user);

            return new ReturnSuccess("changePasswordAdmin", "success", null);

        } catch (Exception e) {
            return new ReturnError("changePasswordAdmin", e.getMessage(), null);
        }
    }

    @GetMapping("/changePassword/{token}/{newPassword}")
    public Object changePassword(@PathVariable(value="token") String token, @PathVariable(value="newPassword") String newPassword) {

        try {

            User user = userRepository.findUserByPasswordResetToken(token).orElseThrow(()->new UserNotFoundException("token not found: "+token));

            var tokenDate = user.getPasswordResetTokenDate();

            Calendar c = Calendar.getInstance();
            c.setTime(tokenDate);
            c.add(Calendar.HOUR,1);

            if (c.getTime().before(new Date())) {
                throw new Exception("token has expired.");
            }

            userFactory.setPassword(user, newPassword);
            
            user.setPasswordResetToken(null);
            user.setPasswordResetTokenDate(null);

            userRepository.save(user);

            return new ReturnSuccess("changePassword", "success", null);

        } catch (Exception e) {
            return new ReturnError("changePassword", e.getMessage(), null);
        }

    }
}
