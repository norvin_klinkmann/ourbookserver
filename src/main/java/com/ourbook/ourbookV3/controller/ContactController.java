package com.ourbook.ourbookV3.controller;

import com.ourbook.ourbookV3.error.ContactNotFoundException;
import com.ourbook.ourbookV3.models.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.ourbook.ourbookV3.repositories.ContactRepository;

import javax.validation.Valid;
import java.util.List;

/**
 *
 * @author norvin_klinkmann
 */

@RestController
public class ContactController {

    @Autowired
    ContactRepository contactRepository;

    // Get All Notes
    @GetMapping("/contact")
    public List<Contact> getAllNotes() {
        return contactRepository.findAll();
    }

    // Create a new Note
    @PostMapping("/contact")
    public Contact createNote(@Valid @RequestBody Contact contact) {
        return contactRepository.save(contact);
    }

    // Get a Single Note
    @GetMapping("/contact/{id}")
    public Contact getNoteById(@PathVariable(value = "id") Long contactId) throws ContactNotFoundException {
        return contactRepository.findById(contactId)
                .orElseThrow(() -> new ContactNotFoundException(contactId));
    }

    // Update a Note
    @PutMapping("/contact/{id}")
    public Contact updateNote(@PathVariable(value = "id") Long contactId,
                           @Valid @RequestBody Contact contactDetails) throws ContactNotFoundException {

        Contact contact = contactRepository.findById(contactId)
                .orElseThrow(() -> new ContactNotFoundException(contactId));

        contact.setUserIdA(contactDetails.getUserIdA());
        contact.setUserIdB(contactDetails.getUserIdB());

        Contact updatedContact = contactRepository.save(contact);

        return updatedContact;
    }

    // Delete a Note
    @DeleteMapping("/contact/{id}")
    public ResponseEntity<?> deleteBook(@PathVariable(value = "id") Long contactId) throws ContactNotFoundException {
        Contact contact = contactRepository.findById(contactId)
                .orElseThrow(() -> new ContactNotFoundException(contactId));

        contactRepository.delete(contact);

        return ResponseEntity.ok().build();
    }
}
