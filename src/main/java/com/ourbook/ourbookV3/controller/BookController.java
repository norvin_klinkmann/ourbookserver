package com.ourbook.ourbookV3.controller;


import com.ourbook.ourbookV3.error.BookNotFoundException;
import com.ourbook.ourbookV3.models.*;
import com.ourbook.ourbookV3.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

/**
 *
 * @author norvin_klinkmann
 */

@RestController
public class BookController {

    @Autowired
    BookRepository bookRepository;

    @Autowired
    BibBookRepository bibBookRepository;

    @Autowired
    BibRepository bibRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    BibMembershipRepository bibMembershipRepository;

    // Get All Notes
    @GetMapping("/books")
    public List<Book> getAllNotes() {
        return bookRepository.findAll();
    }

    // Create a new Note
    @PostMapping("/books/createBook")
    public ResponseEntity<?> createNote(@Valid @RequestBody RestBook restBook) {
        Book book = new Book();
        book.setTitel(restBook.getTitel());
        book.setIsbn(restBook.getIsbn());
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        if (principal instanceof UserDetails) {
//            String currentUsername = ((UserDetails)principal).getUsername();
//        } else {
//            String currentUsername = principal.toString();
//        }
        System.out.println(principal.toString());
        User currentUser = ((CustomUserDetails)principal).getUser();
        book.setUser(currentUser);
        bookRepository.save(book);
        Long bookId = book.getId();
        for (BibMembership bibMembership: bibMembershipRepository.findAllByUserId(currentUser.getId())) {
            BibBook bibBook = new BibBook();
            bibBook.setBibId(bibMembership.getBibId());
            bibBook.setBookId(bookId);
            bibBookRepository.save(bibBook);
        }
        return ResponseEntity.ok().build();
    }

    @GetMapping("/books/getInfoByISBN/{isbn}")
    public RestBook getInfoByISBN(@Valid @RequestBody String isbn){
        return new RestBook("lorem Ipsum"+isbn,isbn);
    }

    // Get a Single Note
    @GetMapping("/books/{id}")
    public Book getNoteById(@PathVariable(value = "id") Long bookId) throws BookNotFoundException {
        return bookRepository.findById(bookId)
                .orElseThrow(() -> new BookNotFoundException(bookId));
    }

    // Update a Note
    @PutMapping("/books/{id}")
    public Book updateNote(@PathVariable(value = "id") Long bookId,
                           @Valid @RequestBody Book bookDetails) throws BookNotFoundException {

        Book book = bookRepository.findById(bookId)
                .orElseThrow(() -> new BookNotFoundException(bookId));

        book.setTitel(bookDetails.getTitel());
        book.setIsbn(bookDetails.getIsbn());
        book.setStatus(bookDetails.getStatus());
        book.setAuthorName(bookDetails.getAuthorName());
        book.setErscheinungsdatum(bookDetails.getErscheinungsdatum());
        book.setSprache(bookDetails.getSprache());
        book.setAuflage(bookDetails.getAuflage());

        return bookRepository.save(book);
    }

    // Delete a Note
    @DeleteMapping("/books/{id}")
    public ResponseEntity<?> deleteBook(@PathVariable(value = "id") Long bookId) throws BookNotFoundException {
        Book book = bookRepository.findById(bookId)
                .orElseThrow(() -> new BookNotFoundException(bookId));

        bookRepository.delete(book);

        return ResponseEntity.ok().build();
    }

}
