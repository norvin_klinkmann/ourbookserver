package com.ourbook.ourbookV3.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SuperadminController {

    @GetMapping("/admin")
    public String hello() {
        return "Hello, Admin!";
    }
}
